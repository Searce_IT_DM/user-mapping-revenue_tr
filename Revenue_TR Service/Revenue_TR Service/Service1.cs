﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.ServiceProcess;
using System.Text;
using System.Timers;
using System.Net;
using System.Xml;
using System.Net.Sockets;
using System.IO;
using System.Net.Mail;
using System.Threading;
using System.Data.Sql;
using System.Data.SqlClient;
using System.Data.OleDb;

namespace Revenue_TR_Service
{

    public partial class Service1 : ServiceBase
    {
        private NetworkStream stream;
        private TcpClient client;
        private byte[] sendbuffer = new byte[512];
        private string data, data2, date, date1, month, year, today_date,  Pvr_Date;
        private byte[] readbuffer = new byte[1024000];
        SqlCommand Cmd= new SqlCommand();
  
        StreamWriter wr;
        SqlDataAdapter da = new SqlDataAdapter();
        System.Data.DataTable Dt = new System.Data.DataTable();
        System.Data.DataTable dt = new System.Data.DataTable();
        DataSet ds = new DataSet();
        public string  ConnectionString, Qry;
        private int InsertCounter = 0, ErrorFlag = 0;
        SqlConnection conn = new SqlConnection();
      
        private int bytecount;
        System.Timers.Timer timer1;
        string IP, UserId, Password,  LogFilePath, str, DownloadFilePath, FilePath, File_Name, NewFilePath, ArchivePath, ErrorFilePath;
        int Port,  RuntimeFrequency, WorkingHours,  Process_Flag = 0 ;
        string  MailServerUser, MailServerPassword, MailTo, MailCC, MailCCERROR;
        public Service1()
        {
            InitializeComponent();
        }

        protected override void OnStart(string[] args)
        {
            sendbuffer = new byte[512];
            readbuffer = new byte[1024000];
            timer1 = new System.Timers.Timer();
            timer1.Elapsed += new System.Timers.ElapsedEventHandler(MyTimer_Elapsed);
            try
            {
               

                if (ReadXmlConfig())
                {
                    writeLog("..................Service Started..................", null);
                    writeLog("notify : ReadXmlConfig done.", null);

                
                    timer1.Interval = RuntimeFrequency;
                    timer1.Enabled = true;
                    timer1.Start();


                }
            }
            catch (Exception Ex1)
            {
                writeLog("Error (EntryPoint.OnStart) : ", Ex1);
                SendEmail("Error (EntryPoint.OnStart) || " + DateTime.Now.ToString(), true);

            }

        }

        public void SendEmail(string MailBody, bool IsError)
        {
            string CCEmail = "";

            if (MailServerUser != "" || MailServerPassword != "")
            {
                try
                {
                    if (IsError)
                        CCEmail = MailCCERROR;
                    else
                        CCEmail = MailCC;
                    //MailAddress toAddress = new MailAddress(ToID);
                    MailAddress fromAddress = new MailAddress(MailServerUser);
                    MailMessage mm = new MailMessage();//fromAddress, toAddress);
                    mm.From = fromAddress;
                    mm.To.Add(MailTo);
                    mm.CC.Add(CCEmail);
                    mm.Subject = "User Mapping Service - " + (IsError ? "Error" : "Notify");
                    mm.Body = MailBody;
                    mm.IsBodyHtml = true;
                    mm.BodyEncoding = System.Text.Encoding.UTF8;
                    //mm.CC.Add(CcID);
                    string smtpHost = "smtp.gmail.com";
                    string userName = MailServerUser;//sending Id
                    string password = MailServerPassword;
                    System.Net.Mail.SmtpClient mClient = new System.Net.Mail.SmtpClient();
                    mClient.Port = 587;
                    mClient.EnableSsl = true;

                    mClient.UseDefaultCredentials = false;
                    mClient.Credentials = new NetworkCredential(userName, password);
                    mClient.Host = smtpHost;
                    mClient.DeliveryMethod = System.Net.Mail.SmtpDeliveryMethod.Network;
                    mClient.Send(mm);
                }
                catch (Exception Ex1)
                {
                    writeLog("Error(SendEmail)", Ex1);

                }
            }
        }
        public bool ReadXmlConfig()
        {

            try
            {
                XmlDocument doc = new XmlDocument();
                doc.Load(appPath + "\\Revenue_TR.xml");

                IP = doc.SelectNodes("/Revenue/IP").Item(0).InnerText.ToString();
                Port = int.Parse(doc.SelectNodes("/Revenue/Port").Item(0).InnerText);
                UserId = doc.SelectNodes("/Revenue/UserId").Item(0).InnerText.ToString();
                Password = doc.SelectNodes("/Revenue/Password").Item(0).InnerText.ToString();
                LogFilePath = doc.SelectNodes("/Revenue/LogFilePath").Item(0).InnerText.ToString();

                MailServerUser = doc.SelectNodes("/Revenue/MailServerUser").Item(0).InnerText.ToString();
                MailServerPassword = doc.SelectNodes("/Revenue/MailServerPassword").Item(0).InnerText.ToString();
                MailTo = doc.SelectNodes("/Revenue/MailTo").Item(0).InnerText.ToString();
                MailCC = doc.SelectNodes("/Revenue/MailCC").Item(0).InnerText.ToString();
                MailCCERROR = doc.SelectNodes("/Revenue/MailCCERROR").Item(0).InnerText.ToString();
                WorkingHours = int.Parse(doc.SelectNodes("/Revenue/WorkingHours").Item(0).InnerText);
                //UploadHour = int.Parse(doc.SelectNodes("/Revenue/UploadHour").Item(0).InnerText);
                RuntimeFrequency = int.Parse(doc.SelectNodes("/Revenue/RuntimeFrequency").Item(0).InnerText);
                ArchivePath = doc.SelectNodes("/Revenue/ArchivePath").Item(0).InnerText.ToString();
                DownloadFilePath = doc.SelectNodes("/Revenue/DownloadFilePath").Item(0).InnerText.ToString();
                ErrorFilePath = doc.SelectNodes("/Revenue/ErrorFilePath").Item(0).InnerText.ToString();
                FilePath = doc.SelectNodes("/Revenue/FilePath").Item(0).InnerText.ToString();

               conn.ConnectionString = doc.SelectNodes("/Revenue/ConnectionString").Item(0).InnerText.ToString();
               
             
                return true;
            }
            catch (Exception ex)
            {
                writeLog("Error (ReadXmlConfig)", ex);
                SendEmail("Error (ReadXmlConfig) || " + DateTime.Now.ToString(), true);
                return false;
            }


        }

        public string appPath
        {
            get
            {
                return System.AppDomain.CurrentDomain.BaseDirectory;
            }
        }


        void MyTimer_Elapsed(object sender, ElapsedEventArgs e)
        {
            try
            {


                timer1.Stop();

                if (DateTime.Now.Hour == WorkingHours)
                {
                    if (Process_Flag == 0)
                    {
                        writeLog("Enter in Time", null);
                        Process_Revenue();
                    }


                }
                if (DateTime.Now.Hour != WorkingHours)
                {
                    Process_Flag = 0;
                }

                GetFile();
                timer1.Start();
            }
            catch (Exception Ex1)
            {
                writeLog("Error (EntryPoint.MyTimer_Elapsed) : ", Ex1);
                SendEmail("Error (EntryPoint.MyTimer_Elapsed) : ", true);
            }
        }
        public void writeLog(string wrl_data, Exception Excp)
        {
            //writeLogData(data, Excp);

            try
            {
                using (FileStream fsL1 = new FileStream(LogFilePath, FileMode.Append, FileAccess.Write))//+ "\\"
                {
                    wr = new StreamWriter(fsL1);
                    str = DateTime.Now.ToString("yyyy-MMM-dd HH:mm:ss") + " | " + wrl_data;
                    if (Excp != null)
                        str += " ==> " + Excp.Message;
                    wr.WriteLine(str);
                    wr.Close();
                    wr.Dispose();
                    fsL1.Close();
                    fsL1.Dispose();
                    if (Excp != null)
                    {
                        SendEmail(str + Environment.NewLine + Excp.StackTrace, true);
                    }
                }
            }
            catch (Exception Ex1)
            {
                SendEmail("Error : writeLog()" + Ex1, true);
            }
        }

        protected override void OnStop()
        {

            try
            {
                writeLog("..................Service Stopped..................", null);
            }
            catch (Exception Ex1)
            {
                writeLog("Error (EntryPoint.OnStop) : ", Ex1);
            }
        }

        private void Send(string Text)
        {
            writeLog("Send : " + Text, null);
            sendbuffer = Encoding.ASCII.GetBytes(Text);
            stream.Write(sendbuffer, 0, sendbuffer.Length);
            Just_Sleep();
        }

        private void Just_Sleep()
        {
            Thread.Sleep(1000);
        }

        private void Tab()
        {
            Send("\t");                         //Tab
            Just_Sleep();
        }

        private void ShortSleep()
        {
            // Read();
            data2 = null;
            data2 = data;

            while (data2 == data)
            {

                Send("");       //Read() will not work with the same screen !!
                Thread.Sleep(1000);
                Read();
            }
        }

        private void Wait(int time)
        {
            Thread.Sleep(time);
        }

        private void Read()
        {
            data = null;
            bytecount = stream.Read(readbuffer, 0, readbuffer.Length);
            data = Encoding.ASCII.GetString(readbuffer);
        }


        private void Delete()
        {
            Send("\x1B[3~");                    //Delete
            Just_Sleep();
        }

        private void Process_Revenue()
        {
            date = DateTime.Now.ToString("dd");
            month = DateTime.Now.ToString("MM");
            year = DateTime.Now.ToString("yyyy");
            date1 = DateTime.Now.AddDays(-2).ToString("dd");
            today_date = year + month + date;
            Pvr_Date = year + month + date1;

            try
            {

                client = new TcpClient(IP, Port);
                stream = client.GetStream();

                for (int i = 0; i < 5; i++)
                {
                    Just_Sleep();
                }

                Enter();

                if (data.Contains("Class is now available"))
                {

                    Send(UserId);                       //UserID
                    Tab();
                    Send(Password);                     //Password
                    Enter();

                    writeLog("class is now available", null);

                }
                else
                {
                    writeLog("class is not available", null);
                    SendEmail("class is not available", true);
                    goto Exit;
                }

                if (data.Contains("Password not correct"))
                {
                    writeLog("notify : Password Not Correct", null);

                }

                else if ((data.Contains("allocated to another job") || data.Contains("User library list limited to 25 libraries") || data.ToUpper().Contains("DAYS UNTIL PASSWORD EXPIRES")))
                {

                    if (data.ToUpper().Contains("DAYS UNTIL PASSWORD EXPIRES"))
                    {
                        Enter();

                        if (data.Contains("Press Enter to continue"))
                        {
                            Enter();
                        }
                    }


                    Enter();

                    if (data.Contains("Master Applications Menu"))
                    {
                        //writeLog("1", null);
                    }
                    else
                    {
                        //writeLog("2", null);
                        Enter();

                    }
                }
                if (data.Contains("Master Applications Menu"))
                {
                    Send("60");
                    Enter();
                    Enter();
                    if (data.Contains("WORK WITH PRINTED OUTPUT") && data.Contains("Type option, press enter"))
                    {
                        string data1;
                        data1 = null;
                        if (data.Contains("QPJOBLOG"))
                        {
                            data1 = data.Substring(data.IndexOf("QPJOBLOG") + 8, data.Length - (data.IndexOf("QPJOBLOG") + 8));

                            while (data1.Contains("QPJOBLOG"))
                            {
                                data1 = data.Substring(data.IndexOf("QPJOBLOG") + 8, data.Length - (data.IndexOf("QPJOBLOG") + 8));
                                Just_Sleep();
                                Send("4");
                                Enter();
                                Enter();
                            }
                        }
                        Send("\x1BOR");         //F3
                        Enter();
                    }
                    Send("\x1BOQ");                         //F2
                    ShortSleep();
                }
                else
                {
                    writeLog("Error : Not Getting Master Applications Menu", null);
                    SendEmail("Error : Not Getting Master Applications Menu || " + DateTime.Now.ToString(), true);
                    goto Exit;
                }
                if (data.Contains("Select an EXTEND Menu"))
                {
                    Send("Query");
                    Enter();
                }
                else
                {
                    writeLog("Error : Not Getting EXTEND Option", null);
                    SendEmail("Error : Not Getting EXTEND Option || " + DateTime.Now.ToString(), true);
                    goto Exit;
                }
                if (data.Contains("Query Menu") || data.Contains("PRODUCTION"))
                {
                    Send("1");
                    Enter();
                }
                else
                {
                    writeLog("Error : Not Getting Query Menu", null);
                    SendEmail("Error : Not Getting Query Menu || " + DateTime.Now.ToString(), true);
                    goto Exit;
                }
                if (data.Contains("Work with Queries"))
                {
                    Send("2");
                    Send("REVENUE_TR");
                    Send("IAHISAGP");
                    Just_Sleep();
                    Just_Sleep();
                    Enter();
                    Just_Sleep();

                }
                else
                {
                    writeLog("Error : Not Getting Work with Queries Screen", null);
                    SendEmail("Error : Not Getting Work with Queries Screen || " + DateTime.Now.ToString(), true);
                    goto Exit;
                }

                if (data.Contains("Define the Query"))
                {
                    Tab();
                    Tab();
                    Tab();
                    Send("1");
                    Enter();

                }
                else
                {
                    if (data.Contains("Query REVENUE_TR in IAHISAGP in use, cannot be shared"))
                    {
                        writeLog("Query REVENUE_TR in IAHISAGP in use, cannot be shared", null);
                        SendEmail("Query REVENUE_TR in IAHISAGP in use, cannot be shared || " + DateTime.Now.ToString(), true);
                        goto Exit;
                    }
                    else
                    {
                        writeLog("Error : Not Getting Define the Query Screen", null);
                        SendEmail("Error : Not Getting Define the Query Screen || " + DateTime.Now.ToString(), true);
                        goto Exit;
                    }
                }

                if (data.Contains("Select Records"))
                {
                    for (int i = 0; i < 14; i++)
                    {
                        Tab();
                    }
                    for (int j = 0; j < 21; j++)

                    { Delete(); }

                    Send("'" + Pvr_Date + "' '" + today_date + "'");

                    Enter();
                    Enter();
                    if (data.Contains("Define the Query") && data.Contains("Select options, or press F3 to save"))
                    {
                        Send("\x1BOR"); //F3
                        ShortSleep();
                    }
                    else
                    {
                        writeLog("Error : Not Getting Define the Query Screen", null);
                        SendEmail("Error : Not Getting Define the Query Screen || " + DateTime.Now.ToString(), true);
                        goto Exit;
                    }

                    if (data.Contains("Exit this Query"))
                    {
                        Tab();
                        Send("2");
                        Enter();
                    }
                    else
                    {
                        writeLog("Error : Not Getting  Exit this Query Screen", null);
                        SendEmail("Error : Not Getting  Exit this Query Screen || " + DateTime.Now.ToString(), true);
                        goto Exit;
                    }
                    if (data.Contains("Work with Queries") && data.Contains("Query option processing completed successfully."))
                    {
                        Send("9");
                        Just_Sleep();
                        Send("\r");
                        // data = null;
                        Just_Sleep();
                        Wait(1800000);
                        //x1:         Read();

                        //    if (data.Contains("File REVENUE_TR in IAHISAGP was replaced."))
                        //    //{break;}
                        //    {
                        //writeLog(" File generated", null);
                       //  SendEmail("UserMapping File generated successfully : " + DateTime.Now.ToString(), false);
                        SetIsActive(1);
                        Process_Flag = 1;
                        // Wait(1200000);
                        Read();


                        //}



                        // else 
                        if (data.Contains(",Cannot replaced"))
                        {

                            writeLog("Error : File Cannot replaced", null);
                            SendEmail("Error : Not Getting  Exit this Query Screen || " + DateTime.Now.ToString(), true);
                            goto Exit;
                        }


                        //else {
                        //    Send("9");
                        //    Just_Sleep();
                        //    Send("REVENUE_TR");
                        //    Just_Sleep();
                        //    Send("IAHISAGP");
                        //    Just_Sleep();
                        //    Send("\r");
                        //    Just_Sleep();
                        //    Wait(600000);
                        //    if (DateTime.Now.Hour == WorkingHours + 1)
                        //    {
                        //        goto Exit;
                        //    }
                        //    else
                        //    {
                        //        goto x1;
                        //    }

                        // }


                    }
                    else
                    {
                        writeLog("Error : Not Getting Work with Queries Screen", null);
                        SendEmail("Error : Not Getting  Exit this Query Screen || " + DateTime.Now.ToString(), true);
                        goto Exit;
                    }
                }
                else
                {
                    writeLog("Error : Not Getting Select Records Screen", null);
                    SendEmail("Error : Not Getting  Select Records Screen || " + DateTime.Now.ToString(), true);
                    goto Exit;
                }
            Exit:
                ;
            }
            catch (Exception ex)
            {
                writeLog("Error (Process_UserMapping()) : " + ex + "|" + DateTime.Now.ToString(), null);
                SendEmail("Error : Not Getting  Select Records Screen" + DateTime.Now.ToString(), true);

            }
        }



        private void SetIsActive(int isactive)
        {
            string qry = "";
            try
            {
                if (isactive == 1)
                {
                    qry = "Update LIVE_METADATA..ofsreports set isactive = 1 where Name = 'UserMapping'";
                }
                if (isactive == 0)
                {
                    qry = "Update LIVE_METADATA..ofsreports set isactive = 0 where Name = 'UserMapping'";
                }

                connection_open();
               // Cmd = new SqlCommand(qry, conn);
                Cmd.CommandText = qry;
                Cmd.ExecuteNonQuery();
                conn.Close();
                ErrorFlag = 0;
            }
            catch (Exception ex)
            {
                writeLog("Error (SetIsActive()) : " + ex + "|" + DateTime.Now.ToString(), null);
                SendEmail("Error (SetIsActive()) : " + ex + "||" + DateTime.Now.ToString(), true);
                ErrorFlag = 1;
            }
        }

        private void Enter()
        {
            Send("\r");                         //ENTER
            ShortSleep();
        }

        private void GetFile()
        {
            try
            {
                date = DateTime.Now.ToString("dd");
                month = DateTime.Now.ToString("MMM");
                year = DateTime.Now.ToString("yyyy");
                DirectoryInfo dir = new DirectoryInfo(DownloadFilePath);
                {
                    foreach (FileInfo file in dir.GetFiles())
                    {
                        File_Name = file.Name;
                        if (File_Name.Contains("UserMapping") && File_Name.Contains(".txt"))
                        {
                          //  File_Name = "UserMapping" + date + "-" + month + "-" + year;
                           NewFilePath = DownloadFilePath + File_Name;
                            File.Move(NewFilePath, FilePath + file.Name);
                            writeLog("in Get FIle", null);
                            if (initaitalize())
                            {
                                dt = Read_TabDelimated(FilePath + file.Name);

                                if (ErrorFlag == 0)
                                {
                                    savetoDb(dt);
                                    if (ErrorFlag == 0)
                                    {
                                        updateDB();
                                        if (ErrorFlag == 0)
                                        {
                                            MoveToArchive(FilePath + File_Name, ArchivePath);

                                        }

                                    }

                                }
                                else
                                { goto Exit1; }
                            }
                            else { writeLog("Not Able to Initialize",null); }
                        }
                    Exit1:
                        ;
                    }
                }
            }

            catch (Exception ex)
            {
                writeLog("Error (GetFile) : ", ex);
                SendEmail("Error (GetFile) : ", true);
            }
        }

        private bool initaitalize()
        {
            try
            {
                connection_open();
                Qry = "Select top 0 * from ClassInvoicesTemp";
                Cmd.CommandText = Qry;
                da = new SqlDataAdapter(Cmd);
                da.Fill(ds);
                dt = ds.Tables[0];
                conn.Close();
                return true;
            }
            catch (Exception ex)
            {
                writeLog("Error in Initializing Datatable" + ex,null);
                return false;
            }

        }
        private void savetoDb(System.Data.DataTable Dt)
        {
            try
            {
                connection_open();
                string qry3 = "truncate  table ClassInvoicesTemp";
                Cmd.CommandText = qry3;
                Cmd.ExecuteNonQuery();
                Cmd.CommandTimeout = 300000;


                InsertCounter = Dt.Rows.Count;
                DataTable distinctTable = Dt.DefaultView.ToTable( /*distinct*/ true);
                writeLog("Count" + Dt.Rows.Count, null);

                InsertCounter = distinctTable.Rows.Count;

                using (SqlBulkCopy s = new SqlBulkCopy(conn))
                {
                    s.DestinationTableName = "ClassInvoicesTemp";

                    s.WriteToServer(distinctTable);
                }
                ds.Tables.Remove(dt);
                Dt.Clear();
                conn.Close();
                writeLog("Inserted in DB", null);
                ErrorFlag = 0;
            }
            catch (Exception ex)
            {
                if (!ex.Message.Contains("A transport-level error has occurred"))
                {
                    writeLog("Error : savetoDb() : ", ex);
                    SendEmail("Error : savetoDb() : " + ex + "||", true);
                    //MoveToArchive(FilePath + file.Name, ErrorFilePath);
                    ErrorFlag = 1;
                    MoveToArchive(FilePath + File_Name, ErrorFilePath);
                    Dt = null;
                }

            }
            finally
            {

                Dt = null;
            }

        }
        private void connection_open()
        {
            try
            {
                if (conn.State != ConnectionState.Open)
                {
                    conn.Close();
                    conn.Open();
                    Cmd.Connection = conn;
                    ErrorFlag = 0;
                }
            }
            catch (Exception e)
            {
                if (!e.Message.Contains("A transport-level error has occurred"))
                {
                    writeLog("Database Connection Error", e);
                    SendEmail("Database Connection Error", true);
                    ErrorFlag = 1;
                    MoveToArchive(FilePath + File_Name, ErrorFilePath);
                }
            }
        }
        private Boolean updateDB()
        {
            try
            {
                string qry2;
                connection_open();
                qry2 = "update ClassInvoices set MAWB=Cit.MAWB, HAWB=Cit.HAWB,SubHAWB=Cit.SubHAWB,ClassUserID=Cit.ClassUserID from  ClassInvoices Ci join ClassInvoicesTemp Cit on (Ci.InvoiceNo=Cit.InvoiceNumber) where Ci.ClassUserID is null";
                Cmd.CommandText = qry2;
                Cmd.ExecuteNonQuery();
                Cmd.CommandTimeout = 300000;
                conn.Close();
                writeLog("UPdated  DB", null);
                SendEmail("UserMapping File generated successfully : " + DateTime.Now.ToString() + Environment.NewLine + "Total Records Instertd in ClassInvoicesTemp=" + InsertCounter, false);

                SetIsActive(0);

                ErrorFlag = 0;
                return true;

            }

            catch (Exception ex)
            {
                if (!ex.Message.Contains("A transport-level error has occurred"))
                {
                    writeLog("Error : UpdateDB() : ", ex);
                    SendEmail("Error : UpdateDB() : " + ex + "||", true);
                    ErrorFlag = 1;
                    MoveToArchive(FilePath + File_Name, ErrorFilePath);

                }
                return false;
            }

        }
        public DataTable Read_TabDelimated(string FileName)
        {

            try
            {
                using (TextReader tr = File.OpenText(FileName))
                {

                  

                    string strLine = string.Empty;
                    string[] arrColumns = null;
                    while ((strLine = tr.ReadLine()) != null)
                    {
                        arrColumns = strLine.Split('\t');
                      
                        dt.Rows.Add(arrColumns);


                    }
                    tr.Close();
                }


                return dt;

            }

            catch (Exception ex)
            {

                writeLog("Error in Rading Data" + ex,null);
                return null;
            }

        }
        //public void Excel_Read()
        //{
        //    try
        //    {
        //        fileName = FilePath + File_Name;

        //        string connectionString = "provider=Microsoft.Jet.OLEDB.4.0;Data Source=" + fileName + ";Extended Properties=Excel 8.0";
        //        using (var Excelconn = new OleDbConnection(connectionString))
        //        {
        //            Excelconn.Open();
        //            var sheets = Excelconn.GetOleDbSchemaTable(System.Data.OleDb.OleDbSchemaGuid.Tables, new object[] { null, null, null, "TABLE" });
        //            using (var Cmd1 = Excelconn.CreateCommand())
        //            {
        //                Cmd1.CommandText = "select Distinct * from [UserMapping" + date + "-" + month + "-" + year + "$]";
        //                var adapter = new OleDbDataAdapter(Cmd1);
        //                ds.Tables.Remove(Dt);
        //                adapter.Fill(ds);
        //                Cmd1.CommandTimeout = 300000;
        //             }
        //            Excelconn.Close();

        //        }
        //        writeLog("Excel Read Done", null);
        //        ErrorFlag = 0;

        //    }
        //    catch (Exception ex)
        //    {
        //        writeLog("Can not open the " + File_Name, null);
        //        SendEmail("Can not open the " + File_Name + ex + "||", true);
        //        ErrorFlag = 1;
        //        MoveToArchive(FilePath + File_Name, ErrorFilePath);


        //    }

        //}
        private void MoveToArchive(string SourcePath, string DestPath)
        {
            string TempFileName = File_Name;
            writeLog("MoveToArchive() called", null);
            try
            {
                if (File.Exists(DestPath + File_Name))
                {
                    TempFileName = File_Name.Insert(File_Name.LastIndexOf("."), DateTime.Now.ToString("dd-MMM-yyyy_hhmmsstt"));
                }
                File.Move(SourcePath, DestPath + TempFileName);

                writeLog("MoveToArchive() :" + "Source :" + SourcePath + File_Name + "Destination" + DestPath + TempFileName, null);
            }
            catch (Exception Ex1)
            {
                writeLog("Error (MoveToArchive) : ", Ex1);
                SendEmail("Error (MoveToArchive) : " + Ex1 + "||", true);
            }
        }




    }
}
